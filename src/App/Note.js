import React, { Component } from 'react';

class Note extends Component{
    d = new Date();
    
    render(){
        const styles = {
            all:{
                position:'relative',
                border:' 1px solid black',
                marginLeft:'150px',
                marginRight:'150px',
                marginBottom:'10px',
                
            }
        }
        return(
            <div style={styles.all}>
                <div>
                    <p>Note : {this.props.children}</p>
                    <p>Date : {this.d.getDate()+"-"+this.d.getMonth()+"-"+this.d.getFullYear()+" "+this.d.getHours()+":"+this.d.getMinutes()+":"+this.d.getSeconds()+""}</p>
                    <p>{this.props.id}</p>
                </div>
                <div>
                    <button onClick={this.props.delete} style={{margin:'0px 0px 10px 0px'}}>Delete</button>
                </div>
            </div>
        );
    }

}

export default Note;